# Pengenalan Design Pattern pada FrontEnd Development

## Daftar Isi
- [Apa itu Design Pattern?](#apa-itu-design-pattern)
- [Mengapa Design Pattern Penting?](#mengapa-design-pattern-penting)
- [Beberapa Contoh Design Pattern](#beberapa-contoh-design-pattern)
- [Bacaan Lanjut](#bacaan-lanjut)

Dalam pengembangan perangkat lunak, **design pattern** adalah solusi umum untuk masalah yang sering muncul. Design pattern membantu pengembang untuk merancang kode yang efisien, mudah dipahami, dan dapat diadaptasi dengan baik. Menerapkan design pattern memungkinkan pengembang untuk menggunakan pendekatan teruji dan terstruktur dalam menanggapi permasalahan umum.

## Apa itu Design Pattern? <a name="apa-itu-design-pattern"></a>

**Design pattern** adalah suatu pendekatan atau templat solusi yang dapat diterapkan pada permasalahan desain perangkat lunak tertentu. Mereka bukanlah kode yang dapat diimplementasikan secara langsung, melainkan pedoman atau ide-ide yang dapat membimbing pengembang dalam merancang solusi yang efektif dan mudah dipelihara.

## Mengapa Design Pattern Penting? <a name="mengapa-design-pattern-penting"></a>

1. **Keterbacaan dan Pemeliharaan Kode:** Design pattern membantu membuat kode lebih terstruktur dan mudah dipahami, memfasilitasi pemeliharaan dan pengembangan berkelanjutan.

2. **Reusabilitas Kode:** Dengan menerapkan design pattern, pengembang dapat memanfaatkan kembali solusi yang sudah teruji dalam berbagai konteks.

3. **Skalabilitas dan Fleksibilitas:** Design pattern membantu mengelola kompleksitas dan meningkatkan fleksibilitas aplikasi terhadap perubahan kebutuhan.

## Beberapa Contoh Design Pattern <a name="beberapa-contoh-design-pattern"></a>

### Repository Design Pattern
**Penjelasan:**

Repository Design Pattern adalah pola desain yang memisahkan logika akses data dari logika bisnis. Ini menyediakan antarmuka umum untuk mengelola data dari berbagai sumber.

**Keuntungan:**
1. *Abstraksi Data:* Memungkinkan pengembang bekerja dengan data tanpa terikat pada sumber data spesifik.
2. *Keterbacaan Kode:* Memisahkan logika akses data membuat kode lebih bersih dan mudah dipelihara.
3. *Fleksibilitas Sumber Data:* Memungkinkan perubahan sumber data tanpa memengaruhi logika bisnis utama.

#### Contoh Penggunaan:
```javascript
class UserRepository {
  constructor(database) {
    this.database = database;
  }

  getUserById(userId) {
    // Logika akses data ke database
    return this.database.query('SELECT * FROM users WHERE id = ?', userId);
  }

  saveUser(user) {
    // Logika penyimpanan data ke database
    return this.database.save('INSERT INTO users (name, email) VALUES (?, ?)', [user.name, user.email]);
  }
}

// Penggunaan Repository
const userRepository = new UserRepository(database);
const user = userRepository.getUserById(123);
userRepository.saveUser(newUser);
```

### Command Design Pattern
**Penjelasan:**

Command Design Pattern memungkinkan Anda mengencapsulasi perintah sebagai objek, memungkinkan antre perintah untuk eksekusi dan mendukung pencelupan operasi dalam objek.

**Keuntungan:**
1. *Pemisahan Klien dan Penerima:* Klien tidak perlu tahu detail implementasi eksekusi perintah, hanya menggunakan objek perintah yang sesuai.
2. *Undo/Redo Operations:* Memungkinkan implementasi sistem undo dan redo dengan menyimpan perintah yang dieksekusi.
3. *Fleksibilitas Eksekusi Perintah:* Objek perintah dapat digunakan untuk menerapkan operasi eksekusi perintah sesuai kebutuhan aplikasi.

#### Contoh Penggunaan:

```javascript
// Command interface
class Command {
  execute() {}
}

// ConcreteCommand
class LightOnCommand extends Command {
  constructor(light) {
    super();
    this.light = light;
  }

  execute() {
    this.light.turnOn();
  }
}

// Receiver
class Light {
  turnOn() {
    console.log('Light is ON');
  }
}

// Invoker
class RemoteControl {
  setCommand(command) {
    this.command = command;
  }

  pressButton() {
    this.command.execute();
  }
}

// Penggunaan Command Design Pattern
const light = new Light();
const lightOnCommand = new LightOnCommand(light);

const remoteControl = new RemoteControl();
remoteControl.setCommand(lightOnCommand);
remoteControl.pressButton(); // Output: Light is ON
```

## Bacaan Lanjut <a name="bacaan-lanjut"></a>
- [Javascript Patterns](https://github.com/lgdevlop/skill-building/blob/master/Javascript/javascript-patterns.md)
